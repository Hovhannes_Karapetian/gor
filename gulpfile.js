

'use strict';

var gulp = require('gulp'),
    browserSync = require("browser-sync"),
    concat = require('gulp-concat'),
    cssmin = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    image = require('gulp-image'),
    merge = require('merge-stream'),
    plumber = require('gulp-plumber'),
    pngquant = require('imagemin-pngquant'),
    pngquant = require('gulp-pngquant'),
    postcss = require('gulp-postcss'),
    prefixer = require('gulp-autoprefixer'),
    reload = browserSync.reload,
    replace = require('gulp-replace'),  
    rigger = require('gulp-rigger'),
    rimraf = require('rimraf'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    spritesmith = require('gulp.spritesmith-multi'),
    uglify = require('gulp-uglify'),
    uncss = require('gulp-uncss'),
    watch = require('gulp-watch');

var path = {
    build: { 
        html: 'dist/',
        js: 'dist/js/',
        css: 'dist/css/',
        img: 'dist/img/',
        sprite_img: 'src/img/',
        sprite_svg: 'src/svg_sprite/',
        sprite_css: 'src/css/',
        assets: 'src/assets/',
        sprite_svg_css: 'src/css/',
        tmp: '../tmp/',
        fonts: 'dist/fonts/'
    },
    src: { 
        html: 'src/*.html', 
        js: 'src/js/main.js',
        jsDop: ['src/js/jquery.fancybox.min.js'],  
        css: 'src/css/main.scss', 
        cssDop: ['src/css/jquery.fancybox.min.css'],  
        img: 'src/img/**/*.*', 
        sprite: 'src/sprite_src/**/*.*',
        sprite_retina: 'src/sprite_src/**/*@2x.png',
        sprite_svg: 'src/svg_icons/*.svg',
       
        fonts: 'src/fonts/**/*.*',
        fontCss: 'src/fonts/**/my.css'
    },
    watch: { 
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        css: 'src/css/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};

var config = {
 server: {
 baseDir: "./dist"
 },
 tunnel: true,    
 host: 'tosei.loc',
 port: 9000,
 logPrefix: "Frontend_Devil"
 };

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html)) 
    .pipe(reload({stream: true}))
    ;
});

gulp.task('js:build', function () {
    gulp.src(path.src.jsDop).pipe(gulp.dest(path.build.js)); 
    gulp.src(path.src.js) 
        .pipe(plumber()) 
        .pipe(rigger()) 
        .pipe(sourcemaps.init()) 
        .pipe(uglify()) 
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js)) 
    .pipe(reload({stream: true})) 
    ;
});

gulp.task('sprite:build', function () {
    var spriteData = gulp.src(path.src.sprite)
            .pipe(spritesmith({
                spritesmith: function (options, sprite) {
                 
                    options.imgName = 'sprite_' + sprite + '.png';
                 
                    options.imgPath = '../img/' + options.imgName;
                    
                    options.cssName = '_sprite_' + sprite + '.scss';
                    options.cssSpritesheetName = 'sp_' + sprite;
                }
            }))
        ;
    spriteData.img
        .pipe(gulp.dest(path.build.sprite_img))
    ;
    spriteData.css
        .pipe(gulp.dest(path.build.sprite_css))
    ;
});

gulp.task('css:build', function () {
    gulp.src(path.src.cssDop).pipe(gulp.dest(path.build.css));
    var fontCssStream = gulp.src(path.src.fontCss)
        .pipe(sourcemaps.init()) 
        .pipe(replace('../font/', '../fonts/my/font/'))
        .pipe(sourcemaps.write({includeContent: false}))
        ;
    var scssStream = gulp.src(path.src.css) 
            .pipe(plumber()) 
            .pipe(sourcemaps.init()) 
            .pipe(sass()) 
            .pipe(prefixer({
                remove: false,   
                browsers: ['last 3 versions']
            })) 
            .pipe(postcss([require('postcss-flexbugs-fixes')]))  
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(reload({stream: true}))
        ;
    var mergedStream = merge(fontCssStream, scssStream)
            .pipe(sourcemaps.init())
            .pipe(concat('main.css'))
            .pipe(sourcemaps.init())
            .pipe(cssmin({level: 2})) 
            .pipe(replace('../font/', '../fonts/my/font/'))
            .pipe(sourcemaps.write({includeContent: false}))     
            .pipe(gulp.dest(path.build.css)) 
        .pipe(reload({stream: true}))
        ;
    return mergedStream;
});

gulp.task('image:build', function () {
    gulp.src(path.src.img) 
    .pipe(imagemin({ 
       progressive: true,
       svgoPlugins: [{removeViewBox: false}],
       use: [pngquant()],
       interlaced: true
    }))
    .pipe(imagemin([
       imagemin.gifsicle({interlaced: true}),
       imagemin.jpegtran({progressive: true}),
       imagemin.optipng({optimizationLevel: 5}),
       imagemin.svgo({plugins: [{removeViewBox: true}]})
    ]))
        .pipe(image({
            pngquant: true,
            optipng: false,
            zopflipng: false,
            advpng: false,
            jpegRecompress: false,
            jpegoptim: false,
            mozjpeg: true,
            gifsicle: true,
            svgo: true
        }))
        .pipe(gulp.dest(path.build.img)) 
    .pipe(reload({stream: true}))
    ;
});

gulp.task('fonts:build', function () {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

// gulp.task('tmp:build', function () {
//     gulp.src(path.src.tmp)
//         .pipe(gulp.dest(path.build.tmp))
// });

gulp.task('build', [
    'html:build',
    'js:build',
    'sprite:build',
    'css:build',
    // 'tmp:build',
    // 'image:build',
    'fonts:build'
]);

gulp.task('watch', function () {
    watch([path.watch.html], function (event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.css], function (event, cb) {
        gulp.start('css:build');
    });
    watch([path.watch.js], function (event, cb) {
        gulp.start('js:build');
    });
    //  watch([path.watch.img], function (event, cb) {
    //      gulp.start('image:build');
    //  });
    watch([path.watch.fonts], function (event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('w', ['watch']);

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['clean', 'build', 'webserver', 'watch']);
// gulp.task('default', ['build', 'webserver', 'watch']);
// gulp.task('default', ['build', 'watch']);