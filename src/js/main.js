//= jquery-3.2.1.min.js
//owl.carousel.min.js
$( document ).ready(function() {
    console.log($( '.work-slider' ))
    $( '.work-slider' ).slick({
        dots: true,
        speed: 300,
        autoplay: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        dotsClass: 'dots-main slick-dots',
        responsive: [
            {
              breakpoint: 1200,
              settings: {
                dots: true,
                speed: 300,
                autoplay: true,
                slidesToShow: 2,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            },
          ]
    });
    $( '.comment-slider' ).slick({
        dots: true,
        speed: 300,
        autoplay: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        dotsClass: 'dots-main slick-dots',
        responsive: [
            {
              breakpoint: 1200,
              settings: {
                dots: true,
                speed: 300,
                autoplay: true,
                slidesToShow: 2,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            },
          ]
        
    });
});